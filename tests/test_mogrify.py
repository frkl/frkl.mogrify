#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `mogrify` package."""

import frkl.mogrify
import pytest  # noqa


def test_assert():

    assert frkl.mogrify.get_version() is not None
