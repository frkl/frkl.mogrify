# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.mogrify"
exe_name = "transmogrify"
project_main_module = "frkl.mogrify"
project_slug = "mogrify"

pyinstaller: Dict[str, Any] = {}
