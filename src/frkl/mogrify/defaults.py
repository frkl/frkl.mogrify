# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


mogrify_app_dirs = AppDirs("mogrify", "frkl")

if not hasattr(sys, "frozen"):
    MOGRIFY_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `mogrify` module."""
else:
    MOGRIFY_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "mogrify")  # type: ignore
    """Marker to indicate the base folder for the `mogrify` module."""

MOGRIFY_RESOURCES_FOLDER = os.path.join(MOGRIFY_MODULE_BASE_FOLDER, "resources")
