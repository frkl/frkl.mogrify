# Installation

There are three ways to install *frkl.mogrify* on your machine. Via a manual binary download, an install script, or installation of the python package.

## Binaries

To install `frkl.mogrify`, download the appropriate binary from one of the links below, and set the downloaded file to be executable (``chmod +x transmogrify``):

  - [Linux](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/transmogrify)
  - [Windows](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/windows/transmogrify.exe)
  - [Mac OS X](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/darwin/transmogrify)


## Install script

Alternatively, use the 'curly' install script for `frkl.mogrify`:

``` console
curl https://gitlab.com/frkl/frkl.mogrify/-/raw/develop/scripts/install/transmogrify.sh | bash
```


This will add a section to your shell init file to add the install location (``$HOME/.local/share/frkl/bin``) to your ``$PATH``.

You might need to source that file (or log out and re-log in to your session) in order to be able to use *transmogrify*:

``` console
source ~/.profile
```


## Python package

The python package is currently not available on [pypi](https://pypi.org), so you need to specify the ``--extra-url`` parameter for your pip command. If you chooose this install method, I assume you know how to install Python packages manually, which is why I only show you an example way of getting *frkl.mogrify* onto your machine:

``` console
> python3 -m venv ~/.venvs/frkl.mogrify
> source ~/.venvs/frkl.mogrify/bin/activate
> pip install --extra-index-url https://pkgs.frkl.io/frkl/dev --extra-index-url https://pkgs.frkl.dev/pypi frkl.mogrify
Looking in indexes: https://pypi.org/simple, https://pkgs.frkl.io/frkl/dev
Collecting frkl.mogrify
  Downloading http://pkgs.frkl.io/frkl/dev/%2Bf/ee3/f57bd91a076f9/frkl.mogrify-0.1.dev24%2Bgd3c4447-py2.py3-none-any.whl (28 kB)
...
...
...
Successfully installed aiokafka-0.6.0 aiopg-1.0.0 ... ... ...
> frkl.mogrify --help
Usage: frkl.mogrify [OPTIONS] COMMAND [ARGS]...
   ...
   ...
```
