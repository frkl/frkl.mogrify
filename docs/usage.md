# Usage


## Getting help

To get information for the `transmogrify` command, use the ``--help`` flag:

{{ cli("transmogrify", "--help") }}
